const express = require('express');
const auth = require("../middleware/auth");
const Task = require("../models/Task");

const router = express.Router();

router.get('/', auth, async (req, res) => {
    try {
        const tasks = await Task.find({user: req.user._id}).populate('user', 'username');
        res.send(tasks);
    } catch (e) {
        res.sendStatus(500);
    }
});



router.post('/', auth, async (req, res) => {

    if (!req.body.user || !req.body.title) {
        return res.status(400).send({error: 'Data not valid'});
    }

    const TaskData = {
        user: req.body.user,
        title: req.body.title,
        description: req.body.description || null,
        status: req.body.status,
    };

    const task = new Task(TaskData);

    try {
        await task.save();
        res.send(task);
    } catch {
        res.status(400).send({error: "Data not valid"})
    }

});


router.delete('/:id', auth, async (req, res) => {
    try {

        const task = await Task.findOneAndDelete({user: req.user._id, _id: req.params.id});

        if (task) {
            res.send(`Task '${task.title}'removed`);
        } else {
            res.status(404).send({error: 'Task not found'});
        }

    } catch (e) {
        res.sendStatus(500);
    }
})


router.put('/:id', auth, async (req, res) => {
    try {
        const task = await Task.findOneAndUpdate({user: req.user._id, _id: req.params.id}, req.body);

        if (task) {
            res.send(`Task '${task.title}' updated ${task}`);
        } else {
            res.status(404).send({error: 'Task not found'});
        }

    } catch (e) {
        res.sendStatus(500);
    }
});


module.exports = router;
